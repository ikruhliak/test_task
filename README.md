$ sudo groupadd docker
$ sudo usermod -ag docker $user
$ sudo chmod +x /usr/local/bin/docker-compose
$ su - ${USER}
$ sudo apt update
$ cd ./backend/src/
$ sudo npm install
$ cd ../../frontend/src/
$ sudo yarn install
$ cd ../../
$ docker-compose up -d
